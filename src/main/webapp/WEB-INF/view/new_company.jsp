<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <style><%@include file="/resources/css/style.css"%></style>
</head>

<body>
    <div align="center">
        <h2>New company</h2>
        <form:form action="/companies/save" method="post" modelAttribute="company">
            <table>
                <tr>
                    <td>Name: </td>
                    <td><form:input path="name"/></td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td><form:input path="status"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Save"/></td>
                </tr>
            </table>
        </form:form>
    </div>
    <div>
        <a href="/companies">Back</a>
    </div>
</body>
</html>