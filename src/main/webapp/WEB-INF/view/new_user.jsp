<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <style><%@include file="/resources/css/style.css"%></style>
</head>

<body>
    <h2>New User</h2>
    <div align="center">
        <form:form action="/users/save" method="post" modelAttribute="user">
            <table>
                <tr>
                    <td>Name: </td>
                    <td><form:input path="name"/></td>
                </tr>
                <tr>
                    <td>Company: </td>
                    <td>
                        <form:select path="company">
                            <c:forEach var="company" items="${companies}">
                                <form:option value="${company.id}">${company.name}</form:option>
                            </c:forEach>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Save"/></td>
                </tr>
            </table>
        </form:form>
    </div>
    <div>
        <a href="/users">Back</a>
    </div>
</body>
</html>