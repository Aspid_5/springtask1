<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <style><%@include file="/resources/css/style.css"%></style>
</head>

<body>
    <div align="center">
        <h2>Edit user</h2>
        <form:form action="/users/updateandsave" method="post" modelAttribute="user">
            <table>
                <tr>
                    <td>ID: </td>
                    <td>${user.id}
                        <form:hidden path="id"/>
                    </td>
                </tr>
                <tr>
                    <td>Name: ${user.name} </td>
                    <td><form:input path="name" /></td>
                </tr>
                <tr>
                    <td>Company: ${user.company.name} </td>
                    <td><form:select path="company">
                        <c:forEach var="company" items="${companies}">
                            <form:option value="${company.id}">${company.name}</form:option>
                        </c:forEach>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Save"></td>
                </tr>
            </table>
        </form:form>
    </div>
    <div>
        <a href="/users">Back</a>
    </div>
</body>
</html>