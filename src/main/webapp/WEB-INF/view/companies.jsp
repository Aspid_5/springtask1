<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style><%@include file="/resources/css/style.css"%></style>
</head>

<body>
    <div align="center">
        <h2>Companies</h2>
        <h3><a href="/companies/create">New company</a></h3>
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Contract Status</th>
                <th>Action</th>
            </tr>
            <c:forEach var="company" items="${companies}">
                <tr>
                    <td>${company.id}</td>
                    <td>${company.name}</td>
                    <td>${company.status}</td>
                    <td>
                        <a href="/companies/update?id=${company.id}">Edit</a>

                        <a href="/companies/delete?id=${company.id}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div>
        <a href="/">Back</a>
    </div>
</body>
</html>