<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style><%@include file="/resources/css/style.css"%></style>
</head>

<body>
    <div align="center">
        <h2>Users</h2>
        <h3><a href="/users/create">New user</a></h3>
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Company</th>
                <th>Action</th>
            </tr>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                    <td>${user.company.name}</td>
                    <td>
                        <a href="/users/update?id=${user.id}">Edit</a>

                        <a href="/users/delete?id=${user.id}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div>
        <a href="/">Back</a>
    </div>
</body>
</html>