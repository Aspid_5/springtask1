<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <style><%@include file="/resources/css/style.css"%></style>
</head>

<body>
    <div align="center">
        <h2>Edit company</h2>
        <form:form action="/companies/updateandsave" method="post" modelAttribute="company">
            <table>
                <tr>
                    <td>ID: </td>
                    <td>${company.id}
                        <form:hidden path="id"/>
                    </td>
                </tr>
                <tr>
                    <td>Name: ${company.name} </td>
                    <td><form:input path="name" /></td>
                </tr>
                <tr>
                    <td>Company: ${company.status} </td>
                    <td><form:input path="status" />
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Save"></td>
                </tr>
            </table>
        </form:form>
    </div>
    <div>
        <a href="/companies">Back</a>
    </div>
</body>
</html>