package org.alch.controllers;

import org.alch.models.Company;
import org.alch.models.User;
import org.alch.services.CompanyService;
import org.alch.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    private final UserService userService;
    private final CompanyService companyService;

    public UserController(UserService userService, CompanyService companyService) {

        this.userService = userService;
        this.companyService = companyService;
    }

    @GetMapping("/users")
    private ModelAndView showAll() {
        ModelAndView modelAndView = new ModelAndView("users");
        List<User> userList = userService.showAll();
        modelAndView.addObject("users", userList);

        return modelAndView;
    }

    @GetMapping("/users/create")
    private ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("new_user");
        List<Company> companyList = companyService.showAll();
        modelAndView.addObject("user", new User());
        modelAndView.addObject("companies", companyList);

        return modelAndView;
    }

    @PostMapping("/users/save")
    private String save(@RequestParam String name, @RequestParam long company) {
        userService.create(name, company);

        return "redirect:/users";
    }

    @PostMapping("/users/updateandsave")
    private String updateAndSave(@RequestParam long id, @RequestParam String name, @RequestParam long company) {
        userService.update(id, name, company);

        return "redirect:/users";
    }

    @GetMapping("/users/delete")
    private String delete(@RequestParam long id) {
        userService.delete(id);

        return "redirect:/users";
    }

   @GetMapping("/users/update")
    private ModelAndView update(@RequestParam long id) {
        ModelAndView modelAndView = new ModelAndView("edit_user");
        User user = userService.get(id);
        modelAndView.addObject("user", user);
        List<Company> companyList = companyService.showAll();
        modelAndView.addObject("companies", companyList);

        return modelAndView;
    }
}