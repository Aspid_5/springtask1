package org.alch.controllers.dto;

public class CompanyDto {

    private long Id;
    private String name;
    private String status;

    public CompanyDto() { }

    public CompanyDto(String name, String status) {
        this.name = name;
        this.status = status;
    }

    public CompanyDto(long id, String name, String status) {
        Id = id;
        this.name = name;
        this.status = status;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
