package org.alch.controllers;

import org.alch.controllers.dto.CompanyDto;
import org.alch.models.Company;
import org.alch.services.CompanyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    private CompanyDto mapToCompanyDto(Company company) { return new CompanyDto(company.getId(), company.getName(), company.getContractStatus().toString()); }

    @GetMapping(value = "/companies")
    private ModelAndView showAll() {
        ModelAndView modelAndView = new ModelAndView("companies");
        List<Company> companyList = companyService.showAll();
        List<CompanyDto> companyDtoList = companyList.stream().map(this::mapToCompanyDto).collect(Collectors.toList());
        modelAndView.addObject("companies", companyDtoList);

        return modelAndView;
    }

    @GetMapping("/companies/create")
    private ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("new_company");
        modelAndView.addObject("company", new CompanyDto());

        return modelAndView;
    }

    @PostMapping("/companies/save")
    private String save(@RequestParam String name, @RequestParam String status) {
        CompanyDto companyDto = new CompanyDto(name, status);
        companyService.create(companyDto.getName(), companyDto.getStatus());

        return "redirect:/companies";
    }

    @PostMapping("/companies/updateandsave")
    private String updateAndSave(@RequestParam long id, @RequestParam String name, @RequestParam String status) {
        CompanyDto companyDto = new CompanyDto(id, name, status);
        companyService.update(companyDto.getId(), companyDto.getName(), companyDto.getStatus());

        return "redirect:/companies";
    }

    @GetMapping("/companies/delete")
    private String delete(@RequestParam long id) {
        companyService.delete(id);

        return "redirect:/companies";
    }

    @GetMapping("/companies/update")
    private ModelAndView update(@RequestParam long id) {
        ModelAndView modelAndView = new ModelAndView("edit_company");
        modelAndView.addObject("company", mapToCompanyDto(companyService.get(id)));

        return modelAndView;
    }
}
