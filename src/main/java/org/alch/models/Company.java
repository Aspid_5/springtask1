package org.alch.models;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Entity
@Table(name = "companies")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    private ContractStatus contractStatus;

    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private List<User> users;

    public Company(String name, String contractStatus) {

        id = new Random().nextLong();
        this.name = name;
        setContractStatus(contractStatus);
    }

    public Company() { }

    public long getId() {
        return id;
    }

    public void setId(long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public ContractStatus getContractStatus() { return contractStatus; }

    public void setContractStatus(String contractStatus) {
        final ContractStatus status = ContractStatus.findByDesc(contractStatus);
        this.contractStatus = status;
    }

    public enum ContractStatus {
        NotConcluded,
        Concluded,
        Terminated;

        private static ContractStatus findByDesc(final String statusCode) {
            return Arrays.stream(ContractStatus.values())
                    .filter(status -> status.toString().equals(statusCode))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Невозможно найти статус с кодом: " + statusCode));
        }
    }
}
