package org.alch.services;

import org.alch.models.Company;
import org.alch.repositories.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> showAll() { return companyRepository.findAll(); }

    public void create(String name, String contractStatus) { companyRepository.save(new Company(name, contractStatus)); }

    public Company get(long id) { return companyRepository.findById(id).get(); }

    public void delete(long id) {
        companyRepository.deleteById(id);
    }

    public void update(long id, String name, String contractStatus) {
        Company company = companyRepository.findById(id).get();
        company.setName(name);
        company.setContractStatus(contractStatus);
        companyRepository.save(company);
    }
}
