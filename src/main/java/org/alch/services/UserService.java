package org.alch.services;

import org.alch.models.User;
import org.alch.repositories.CompanyRepository;
import org.alch.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;

    public UserService(UserRepository userRepository, CompanyRepository companyRepository) {

        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
    }

    public List<User> showAll() { return userRepository.findAll(); }

    public void create(String name, long company) { userRepository.save(new User(name, companyRepository.findById(company).get())); }

    public User get(long id) { return userRepository.findById(id).get(); }

    public void delete(long id) { userRepository.deleteById(id); }

    public void update(long id, String name, long company) {
        User user = userRepository.findById(id).get();
        user.setName(name);
        user.setCompany(companyRepository.findById(company).get());
        userRepository.save(user);
    }
}
