insert into companies(contractstatus, name)
VALUES ('NotConcluded','Mercedes');
insert into companies(contractstatus, name)
VALUES ('NotConcluded', 'Porsche');
insert into companies(contractstatus, name)
VALUES ('Concluded', 'Audi');
insert into companies(contractstatus, name)
VALUES ('Terminated', 'BMW');
insert into companies(contractstatus, name)
VALUES ('Concluded', 'Volkswagen');
insert into companies(contractstatus, name)
VALUES ('Concluded', 'Maybach');
insert into users(name, company_id)
VALUES ('Alexey', 1);
insert into users(name, company_id)
VALUES ('Oleg', 1);
insert into users(name, company_id)
VALUES ('Denis', 1);
insert into users(name, company_id)
VALUES ('Sergey', 1);
insert into users(name, company_id)
VALUES ('Inna', 1);
insert into users(name, company_id)
VALUES ('Elena', 1);
insert into users(name, company_id)
VALUES ('Anastasiya', 1);
insert into users(name, company_id)
VALUES ('Maxim', 2);
insert into users(name, company_id)
VALUES ('Elena', 2);
insert into users(name, company_id)
VALUES ('Ekaterina', 2);
insert into users(name, company_id)
VALUES ('Artyom', 2);
insert into users(name, company_id)
VALUES ('Roman', 2);
insert into users(name, company_id)
VALUES ('Laura', 3);
insert into users(name, company_id)
VALUES ('Alyona', 3);
insert into users(name, company_id)
VALUES ('Svetlana', 3);
insert into users(name, company_id)
VALUES ('Tatyana', 3);
insert into users(name, company_id)
VALUES ('Konstantin', 3);
insert into users(name, company_id)
VALUES ('Mihail', 4);
insert into users(name, company_id)
VALUES ('Vadim', 4);
insert into users(name, company_id)
VALUES ('Maria', 4);
insert into users(name, company_id)
VALUES ('Zahar', 4);
insert into users(name, company_id)
VALUES ('Vasiliy', 4);
insert into users(name, company_id)
VALUES ('Evgeniya', 5);
insert into users(name, company_id)
VALUES ('Oksana', 5);
insert into users(name, company_id)
VALUES ('Sergey', 5);